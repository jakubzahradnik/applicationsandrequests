﻿using Microsoft.AspNetCore.Identity;

namespace AR.Panel.Models;

public class ApplicationUser : IdentityUser
{
}