﻿using AR.Panel.Data;
using Microsoft.EntityFrameworkCore;

namespace AR.Panel
{
    public static class StartupExtensions
    {
        public static IServiceCollection AddDatabase(this IServiceCollection service, string connectionString)
        {
            service.AddDbContext<AppDbContext>(
                n =>
                {
#if DEBUG
                    n.EnableSensitiveDataLogging();
#endif
                    var config =
                    n.UseNpgsql(connectionString);
                    n.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
                });

            return service;
        }
    }
}