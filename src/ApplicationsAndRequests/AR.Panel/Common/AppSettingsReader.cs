﻿using System.Text.Json.Serialization;

namespace AR.Panel.Common;

public class AppSettingsReader : IAppSettingsReader
{
    public AppSettings ReadAppSettings(IConfiguration configuration)
    {
        return new AppSettings(
            configuration["ApiUrl"],
            new ConnectionStrings(
                configuration["ConnectionStrings:DefaultConnection"]));
    }
}

public interface IAppSettingsReader
{
    AppSettings ReadAppSettings(IConfiguration configuration);
}

public class ConnectionStrings
{
    public ConnectionStrings(string defaultConnection)
    {
        DefaultConnection = defaultConnection;
    }

    [JsonPropertyName("DefaultConnection")]
    public string DefaultConnection { get; }
}

public class AppSettings
{
    public AppSettings(string apiUrl, ConnectionStrings connectionStrings)
    {
        ApiUrl = apiUrl;
        ConnectionStrings = connectionStrings;
    }

    [JsonPropertyName("ApiUrl")] public string ApiUrl { get; }

    [JsonPropertyName("ConnectionStrings")]
    public ConnectionStrings ConnectionStrings { get; }
}